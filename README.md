![PartyCenterSoftware](docs/images/pcs-logo.png)

# Getting started with PCS public api - Tutorial

API and swagger documentation are available at [https://api.partycs.com/index.html](https://api.partycs.com/index.html).

## Prerequisites

For this tutorial you will need a valid **pcs-facility-id** key, this key is a GUID identifier available in the Facility Info
page in the PCS software.
This key is mandatory and **MUST** be present in every request as a header called **pcs-facility-id**, for the reporting routes
where we use a company header, you key specify any facility that belongs to that company.

For some routes you may need a **pcs-company-id** key, this key is only available upon support request. For those routes you can
send a header called **pcs-company-id** with the GUID value.

## Tutorials

| Tutorial | Topics |
| ----- | ---- |
| [Tutorial #1](docs/1-using-swagger-ui.md) | Using the swagger UI to send requests. |