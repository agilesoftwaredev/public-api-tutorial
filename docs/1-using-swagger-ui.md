
# Using the swagger UI to send requests.

1. Open the [Swagger UI](https://api.partycs.com/index.html)

![SwaggerUI](images/1-swagger-ui.png)

## Orders endpoint
The orders endpoint only required the **pcs-facility-id** header. It has multiple filters but they are optional.
In order to send a request follow the steps:

1. Click in the *Try it out* button:
    1. ![Try it button](images/1-try-now-btn.png)
1. Scroll to the **pcs-facility-id** header and paste the value from the facility info page
    1. ![Orders request](images/1-orders-request.png)
    1. If the header key is valid you should see a 200 response with the paginated data from the request.

## New Bookings Report
This report required a company id header, so you must fill both headers **pcs-facility-id** and **pcs-company-id**

1. Click in the *Try it out* button and fill both header values.
    1. ![Request](images/1-report-new-request.png)
    1. ![Response](images/1-report-new-response.png)